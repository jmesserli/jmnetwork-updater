/*
 # JMNetwork Updater Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 #################################################################################################*/

package ch.jmnetwork.updater.tools;

import ch.jmnetwork.core.tools.list.ListUtils;
import ch.jmnetwork.core.tools.string.StringUtils;
import ch.jmnetwork.core.tools.xml.BetterNodeList;
import ch.jmnetwork.core.tools.xml.XmlUtils;
import ch.jmnetwork.updater.JMNetworkUpdater;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class VersionParser {

    private Element rootElement;

    private String versionName;
    private ArrayList<String> fileList;
    private ArrayList<String> dependencyList = new ArrayList<>();

    public VersionParser(File xmlFile) {
        Document document = XmlUtils.getXMLDocumentFromFile(xmlFile);
        rootElement = document.getDocumentElement();
        parseVersion();
    }

    private void parseVersion() {
        versionName = XmlUtils.getChildText("name", rootElement);
        fileList = XmlUtils.getAllChildTexts("file", XmlUtils.getFirstChildElementWithName("files", rootElement));

        Node dependencies = XmlUtils.getFirstChildElementWithName("dependencies", rootElement);
        if (dependencies == null) {
            dependencyList = null;
            return;
        }
        BetterNodeList dependencyNodeList = XmlUtils.getAllChildrenWithName("dependency", dependencies);
        for (Node n : dependencyNodeList) {
            dependencyList.add(XmlUtils.getChildText("name", n));
        }
    }

    /**
     * @return null if no dependencies
     */
    public HashMap<String, String> lookupDependencies() {
        return getAllFilesRecursive(new HashMap<String, String>());
    }

    private HashMap<String, String> getAllFilesRecursive(HashMap<String, String> map) {
        for (String file : fileList) {
            if (!map.containsKey(file)) map.put(file, versionName);
        }

        if (hasDependencies()) {
            for (String dependency : dependencyList) {
                VersionParser t_parser = JMNetworkUpdater.getVersionParserForVersion(dependency);
                map = t_parser.getAllFilesRecursive(map);
            }
        }

        return map;
    }

    public String getVersionName() {
        return versionName;
    }

    public ArrayList<String> getFileList() {
        return fileList;
    }

    public ArrayList<String> getDependencyList() {
        return dependencyList;
    }

    public boolean hasDependencies() {
        return dependencyList != null;
    }

    public void printInformation() {
        System.out.println("Version Report");
        System.out.println("==============\n");

        ArrayList<String> description = new ArrayList<>();
        ArrayList<String> value = new ArrayList<>();

        description.add("Version Name:");
        value.add(versionName);

        description.add("Has Dependencies:");
        value.add(hasDependencies() + "");

        System.out.println(StringUtils.getColumnizedString(ListUtils.getListWithArraysOfLists(String.class, description, value)));

        System.out.println("Files:");
        for (String file : fileList) System.out.println(" - " + file);
        System.out.println();

        if (hasDependencies()) {
            System.out.println("Dependencies:");
            for (String dependency : dependencyList) System.out.println(" - " + dependency);
        }
    }
}
