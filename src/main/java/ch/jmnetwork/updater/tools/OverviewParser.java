/*
 # JMNetwork Updater Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 #################################################################################################*/

package ch.jmnetwork.updater.tools;

import ch.jmnetwork.core.tools.list.ListUtils;
import ch.jmnetwork.core.tools.string.StringUtils;
import ch.jmnetwork.core.tools.xml.XmlUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.util.ArrayList;

/**
 * User: joel / Date: 21.01.14 / Time: 21:06
 */
public class OverviewParser {

    private Document document;
    private Element rootelement;
    private ArrayList<String> versionList = new ArrayList<>();
    private String newestVersion;
    private int versionCount;

    public OverviewParser(File overviewXML) {
        document = XmlUtils.getXMLDocumentFromFile(overviewXML);
        rootelement = document.getDocumentElement();
        analyze();
    }

    private void analyze() {
        newestVersion = XmlUtils.getAttributeValue("newest", rootelement);
        versionCount = Integer.parseInt(XmlUtils.getAttributeValue("count", rootelement));

        versionList = XmlUtils.getAllAttributeValues("version", "name", rootelement);
    }

    public ArrayList<String> getVersionList() {
        return versionList;
    }

    public String getNewestVersion() {
        return newestVersion;
    }

    public int getVersionCount() {
        return versionCount;
    }

    public boolean doesVersionExist(String version) {
        return versionList.contains(version);
    }

    public void printInformation() {
        System.out.println("Overview Parser Report");
        System.out.println("======================\n");

        ArrayList<String> description = new ArrayList<>();
        ArrayList<String> value = new ArrayList<>();

        description.add("Version Count (XML):");
        value.add(versionCount + "");

        description.add("Versions Found:");
        value.add(versionList.size() + "");

        description.add("Newest Version:");
        value.add(newestVersion);

        System.out.println(StringUtils.getColumnizedString(ListUtils.getListWithArraysOfLists(String.class, description, value)));
        System.out.println("All Versions:");
        for (String version : versionList) System.out.println(" - " + version);
    }
}
