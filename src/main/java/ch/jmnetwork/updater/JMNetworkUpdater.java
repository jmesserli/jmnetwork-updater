/*
 # JMNetwork Updater Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 #################################################################################################*/

package ch.jmnetwork.updater;

import ch.jmnetwork.core.arguments.ArgumentParser;
import ch.jmnetwork.core.logging.LogLevel;
import ch.jmnetwork.core.logging.Logger;
import ch.jmnetwork.core.tools.network.NetworkUtils;
import ch.jmnetwork.updater.tools.OverviewParser;
import ch.jmnetwork.updater.tools.VersionParser;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class JMNetworkUpdater {

    protected static ArgumentParser parser;
    protected static Logger logger = new Logger(new File("updateLog.log"), "JMNetwork Updater");

    protected static OverviewParser overviewParser;
    protected static VersionParser versionParser;

    protected static URL projectURL;
    protected static String toVersion;
    protected static File installDir;

    public static void main(String[] args) {
        parseArguments(args, "-");

        overviewParser = getOverviewParser();
        if (overviewParser.doesVersionExist(toVersion)) {
            versionParser = getVersionParserForVersion(toVersion);
            downloadVersion();
        } else {
            overviewParser.printInformation();
            logger.log(LogLevel.ERROR, "Version " + toVersion + " doesn't exist! Using newest version " + overviewParser.getNewestVersion());
            if (overviewParser.doesVersionExist(overviewParser.getNewestVersion())) {
                versionParser = getVersionParserForVersion(overviewParser.getNewestVersion());
                downloadVersion();
            } else {
                logger.fail(LogLevel.ERROR, "Newest Version " + overviewParser.getNewestVersion() + " doesn't exist! Contact the repository owner!");
            }
        }
    }

    private static void downloadVersion() {
        LogLevel logLevel = LogLevel.DEBUG;

        long timeStart = System.currentTimeMillis();
        HashMap<String, String> allFiles = versionParser.lookupDependencies();

        logger.log(logLevel, "Dependency lookup complete:");
        for (String file : allFiles.keySet()) {
            logger.log(logLevel, String.format("%s from Version %s", file, allFiles.get(file)));
        }

        for (String file : allFiles.keySet()) {
            File dl = getFileFromVersion(file, allFiles.get(file));
            logger.log(logLevel, String.format("Downloaded file %s (Version %s)", dl.getName(), allFiles.get(dl.getName())));
        }

        long timeEnd = System.currentTimeMillis();
        logger.log(logLevel, "Download for Version " + versionParser.getVersionName() + " completed in " + ((timeEnd - timeStart) / 1000F) + " seconds!");
    }

    private static File getFileFromVersion(String file, String version) {
        return NetworkUtils.simpleDownload(projectURL + "versions/" + version + "/" + file, "./tmp/download/" + file);
    }

    private static String getVersionDescriptionURL(String version) {
        return projectURL + "versions/" + version + "/version-description.xml";
    }

    private static String getVersionOverviewURL() {
        return projectURL + "version-overview.xml";
    }

    private static OverviewParser getOverviewParser() {
        return new OverviewParser(NetworkUtils.simpleDownload(getVersionOverviewURL(), "./tmp/overview.xml"));
    }

    public static VersionParser getVersionParserForVersion(String version) {
        return new VersionParser(NetworkUtils.simpleDownload(getVersionDescriptionURL(version), "./tmp/version-" + version + ".xml"));
    }

    public static VersionParser getVersionParser() {
        return versionParser;
    }

    private static void parseArguments(String[] args, String separator) {
        parser = new ArgumentParser(args, separator);

        if (!parser.isArgumentSet("url") || !(parser.isArgumentSet("tov") || parser.isArgumentSet("toversion")))
            logger.fail(LogLevel.FATAL, getUsage());

        try {
            projectURL = new URL(parser.getArgument("url"));
        } catch (MalformedURLException e) {
            logger.fail(LogLevel.ERROR, "Your URL " + parser.getArgument("url") + " is not correct: " + e.getMessage());
        }

        toVersion = parser.isArgumentSet("tov") ? parser.getArgument("tov") : parser.getArgument("toVersion");

        installDir = parser.isArgumentSet("install") ? new File(parser.getArgument("install")) : new File("./");

        if (!installDir.exists()) installDir.mkdir();
        else if (!installDir.isDirectory()) installDir.mkdir();
    }

    private static String getUsage() {
        return "USAGE: JMNetworkUpdater.jar -url <project url> (-tov | -toversion) <version to get> [-install <directory to install into>]";
    }
}
